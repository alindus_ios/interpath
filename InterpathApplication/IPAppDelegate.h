//
//  IPAppDelegate.h
//  InterpathApplication
//
//  Created by Pragathi on 7/9/13.
//  Copyright (c) 2013 Pragathi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
