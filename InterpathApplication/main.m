//
//  main.m
//  InterpathApplication
//
//  Created by Pragathi on 7/9/13.
//  Copyright (c) 2013 Pragathi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IPAppDelegate class]));
    }
}
